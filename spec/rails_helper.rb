# require database cleaner at the top level
require 'database_cleaner'
require 'spec_helper'

# [...]
Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }
# [...]
RSpec.configuration do |config|
# [...]
config.include RequestSpecHelper, type: :request
# [...]
end

RSpec.configure do |config|
  # [...]
  # configure shoulda matchers to use rspec as the test framework and full matcher libraries for rails
  config.include RequestSpecHelper
  config.include ControllerSpecHelper
  # [...]
  Shoulda::Matchers.configure do |config|
    config.integrate do |with|
      with.test_framework :rspec
      with.library :rails
    end
  end
  # [...]
  # add `FactoryBot` methods
  # config.include FactoryBot::Syntax::Methods

  # start by truncating all the tables but then use the faster transaction strategy the rest of the time.
  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
    DatabaseCleaner.strategy = :transaction
  end

  # start the transaction strategy as examples are run
  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
  # [...]

end